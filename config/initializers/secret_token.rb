# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MusicApp::Application.config.secret_key_base = '28ba632e7a44796acd0bd0b4eb36a8b69c3f252a76f8c2852bc7ce1f777007daff18b239cdc6dd09ee6e14f9c8f8043c15212af7f228909ac7ead59d71b13201'
