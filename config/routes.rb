MusicApp::Application.routes.draw do
  resources :users

  resource :session

  resources :bands do
    resources :albums, only: [:new]
  end

  resources :albums, except: [:new, :index] do
    resources :tracks, only: [:new]
  end

  resources :tracks, except: [:new, :index] do
    resources :notes, only: [:new, :show]
  end

  resources :notes, except: [:new, :show]
end
