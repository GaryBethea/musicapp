class Album < ActiveRecord::Base
  validates :name, :band_id, presence: :true
  validates :recording_style, inclusion: ["Studio", "Live"]

  belongs_to :band
  has_many :tracks, dependent: :destroy
end
