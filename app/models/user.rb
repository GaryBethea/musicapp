class User < ActiveRecord::Base
  validates :email, :password_digest, :session_token, presence: true
  # validates :password, length: { minimum: 6 }, allow_nil: true

  after_initialize :ensure_session_token
  attr_reader :password

  has_many :notes

  def self.generate_session_token # why make this a class method?
    SecureRandom.base64
  end

  def self.find_by_credentials(email, password)
    user = self.find_by_email(email)
    return nil if user.nil?

    if user.is_password?(password)
      user
    else
      nil
    end
  end

  def reset_session_token!
    self.class.generate_session_token
  end

  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  private

  def ensure_session_token
    self.session_token ||= self.class.generate_session_token
    self.save
    self.session_token
  end

end
