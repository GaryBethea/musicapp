class ApplicationController < ActionController::Base
  helper_method :current_user, :logged_in?
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def log_in!(user)
    user.reset_session_token!
    token = user.session_token
    session[:session_token] = token
    @current_user = user
  end

  def logged_in?
    return nil if current_user.nil?

    current_user.session_token == session[:session_token]
  end

  def log_out!
    current_user.reset_session_token!
    session[:session_token] = nil
  end

  def current_user
    @current_user ||= User.find_by_session_token(session[:session_token])
  end

  def ensure_log_in
    unless logged_in?
      flash[:notice] = "Sorry, you have to be logged in to do that."
      redirect_to new_session_url
    end
  end

end
