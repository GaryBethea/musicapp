class NotesController < ApplicationController

  def new
    @track = Track.find(params[:track_id])
    @note = Note.new
    render :new
  end

  def create
    @note = Note.new(note_params)
    @note.user_id = current_user.id
    @track = Track.find(params[:track][:id])
    @note.track_id = @track.id
    @note.save

    redirect_to track_url(@track)
  end

  def show
  end

  def edit
  end

  def update
  end

  def destroy
  end

  private

  def note_params
    self.params.require(:note).permit(:content)
  end

end
