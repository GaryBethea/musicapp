class AlbumsController < ApplicationController
  before_filter :ensure_log_in

  def new
    @album = Album.new
    @band = Band.find(params[:band_id])
    @bands = Band.all

    render :new
  end

  def create
    @album = Album.new(album_params)

    if @album.save
      redirect_to album_url(@album)
    else
      render :new
    end
  end

  def show
    @album = Album.find(params[:id])
    @band = @album.band
  end

  def edit
    @album = Album.find(params[:id])
    @band = @album.band
    @bands = Band.all
    render :edit
  end

  def update
    @album = Album.find(params[:id])

    if Album.update(@album, album_params)
      redirect_to album_url(@album)
    else
      render :edit
    end
  end

  def destroy
    @album = Album.find(params[:id])
    @album.destroy

    redirect_to band_url(@album.band)
  end

  private

  def album_params
    self.params.require(:album).permit(:name, :band_id, :recording_style)
  end

end
