class TracksController < ApplicationController
  before_filter :ensure_log_in

  def new
    @track = Track.new
    @album = Album.find(params[:album_id])
    @albums = Album.all

    render :new
  end

  def create
    @track = Track.new(track_params)

    if @track.save
      redirect_to track_url(@track)
    else
      render :new
    end
  end

  def show
    @track = Track.find(params[:id])
    @album = @track.album
    render :show
  end

  def edit
    @track = Track.find(params[:id])
    @album = @track.album
    @albums = Album.all

    render :edit
  end

  def update
    @track = Track.find(params[:id])

    if Track.update(@track, track_params)
      redirect_to track_url(@track)
    else
      render :edit
    end
  end

  def destroy
    @track = Track.find(params[:id])
    @track.destroy

    redirect_to album_url(@track.album)
  end

  private

  def track_params
    self.params.require(:track).permit(:name, :album_id, :status, :lyrics)
  end

end


